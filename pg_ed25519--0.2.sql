-- complain if script is sourced in psql, rather than via CREATE EXTENSION
\echo Use "CREATE EXTENSION pg_ed25519" to load this file. \quit

CREATE FUNCTION sign(bytea, bytea, bytea)
    RETURNS bytea
    LANGUAGE C
    IMMUTABLE STRICT PARALLEL SAFE
AS
'MODULE_PATHNAME',
'pg_sign';

CREATE FUNCTION sign(text, bytea, bytea)
    RETURNS bytea
    LANGUAGE C
    IMMUTABLE STRICT PARALLEL SAFE
AS
'MODULE_PATHNAME',
'pg_sign';

CREATE FUNCTION verify(bytea, bytea, bytea)
    RETURNS boolean
    LANGUAGE C
    IMMUTABLE STRICT PARALLEL SAFE
AS
'MODULE_PATHNAME',
'pg_verify';

CREATE FUNCTION verify(text, bytea, bytea)
    RETURNS boolean
    LANGUAGE C
    IMMUTABLE STRICT PARALLEL SAFE
AS
'MODULE_PATHNAME',
'pg_verify';
