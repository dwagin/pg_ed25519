#include "postgres.h"
#include <string.h>
#include "fmgr.h"
#include "ed25519/ed25519.h"

#define ED25519_KEY_LENGTH 32
#define ED25519_DIGEST_LENGTH 64

#ifdef PG_MODULE_MAGIC
PG_MODULE_MAGIC;
#endif

PG_FUNCTION_INFO_V1(pg_sign);

Datum pg_sign(PG_FUNCTION_ARGS)
{
	bytea *message, *signature, *public_key, *private_key;
	unsigned message_len;

	message = PG_GETARG_BYTEA_PP(0);
	message_len = VARSIZE_ANY_EXHDR(message);
	public_key = PG_GETARG_BYTEA_PP(1);
	private_key = PG_GETARG_BYTEA_PP(2);

	if (VARSIZE_ANY_EXHDR(public_key) != ED25519_KEY_LENGTH)
		ereport(ERROR,
		        (errcode(ERRCODE_INVALID_PARAMETER_VALUE),
		            errmsg("public_key should have 32 bytes length")));

	if (VARSIZE_ANY_EXHDR(private_key) != ED25519_KEY_LENGTH)
		ereport(ERROR,
		        (errcode(ERRCODE_INVALID_PARAMETER_VALUE),
		            errmsg("private_key should have 32 bytes length")));

	signature = (bytea *) palloc(ED25519_DIGEST_LENGTH + VARHDRSZ);
	SET_VARSIZE(signature, ED25519_DIGEST_LENGTH + VARHDRSZ);

	ed25519_sign((unsigned char *) VARDATA_ANY(signature),
	             (const unsigned char *) VARDATA_ANY(message),
	             message_len,
	             (const unsigned char *) VARDATA_ANY(public_key),
	             (const unsigned char *) VARDATA_ANY(private_key));

	PG_FREE_IF_COPY(message, 0);
	PG_FREE_IF_COPY(public_key, 1);
	PG_FREE_IF_COPY(private_key, 2);

	PG_RETURN_BYTEA_P(signature);
}

PG_FUNCTION_INFO_V1(pg_verify);

Datum pg_verify(PG_FUNCTION_ARGS)
{
	bytea *message, *signature, *public_key;
	unsigned message_len;
	int res;

	message = PG_GETARG_BYTEA_PP(0);
	message_len = VARSIZE_ANY_EXHDR(message);
	signature = PG_GETARG_BYTEA_PP(1);
	public_key = PG_GETARG_BYTEA_PP(2);

	if (VARSIZE_ANY_EXHDR(signature) != ED25519_DIGEST_LENGTH)
		ereport(ERROR,
		        (errcode(ERRCODE_INVALID_PARAMETER_VALUE),
		            errmsg("signature should have 64 bytes length")));

	if (VARSIZE_ANY_EXHDR(public_key) != ED25519_KEY_LENGTH)
		ereport(ERROR,
		        (errcode(ERRCODE_INVALID_PARAMETER_VALUE),
		            errmsg("public_key should have 32 bytes length")));

	res = ed25519_verify((const unsigned char *) VARDATA_ANY(signature),
	                     (const unsigned char *) VARDATA_ANY(message),
	                     message_len,
	                     (const unsigned char *) VARDATA_ANY(public_key));

	PG_FREE_IF_COPY(message, 0);
	PG_FREE_IF_COPY(signature, 1);
	PG_FREE_IF_COPY(public_key, 2);

	if (res) {
		PG_RETURN_BOOL(true);
	}

	PG_RETURN_BOOL(false);
}

