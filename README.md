# pg_ed25519

Version: 0.2

**pg_ed25519** is a PostgreSQL extension for signing and verify signatures via ed25519 algorithm.

**pg_ed25519** is released under the MIT license (See LICENSE file).

### Version Compatability
This code is built with the following assumptions.  You may get mixed results if you deviate from these versions.

* [PostgreSQL](http://www.postgresql.org) 9.4+

### Requirements
* PostgreSQL

### Building

To build you will need to install PostgreSQL server development packages.
On Debian based distributions you can usually do something like this:

    apt-get install -y postgresql-server-dev-all

If you have all of the prerequisites installed you should be able to just:

    make && make install

### Usage
Generate ed25519 keys via openssl:

    openssl genpkey -algorithm ed25519 -outform PEM -out test25519.pem

Extract private key, base64 encoded:

    openssl pkey -outform DER -in test25519.pem | tail -c +17 | head -c 32 | openssl base64

For example it will be

    eT7qaT8vkIgCl6/9EmEDYYEgxA0oOgHc0P6UYzcQN28=

Extract public key, base64 encoded:

    openssl pkey -outform DER -pubout -in test25519.pem | tail -c +13 | head -c 32 | openssl base64

For example it will be

    NZV4l8hck3iUqInENyI+nn5vkW7rqzQg0uiuuZkPnHE=

Open pgsql console

```sql
SELECT ed25519.verify(
               text 'some data for signing',
               ed25519.sign(
                       text 'some data for signing',
                       decode('NZV4l8hck3iUqInENyI+nn5vkW7rqzQg0uiuuZkPnHE=', 'base64'),
                       decode('eT7qaT8vkIgCl6/9EmEDYYEgxA0oOgHc0P6UYzcQN28=', 'base64')
                   ),
               decode('NZV4l8hck3iUqInENyI+nn5vkW7rqzQg0uiuuZkPnHE=', 'base64')
           );
```

Result should be

     verify
    --------
     t
    (1 row)

### Support

File bug reports, feature requests and questions using
[GitLab Issues](https://gitlab.com/dwagin/pg_ed25519/issues)

