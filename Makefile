SRCS = \
    src/ed25519/fe.c \
    src/ed25519/ge.c \
    src/ed25519/sc.c \
    src/ed25519/sha512.c \
    src/ed25519/sign.c \
    src/ed25519/verify.c \
    src/pg_ed25519.c

MODULE_big = pg_ed25519
OBJS = $(SRCS:.c=.o)

EXTENSION = pg_ed25519
DATA = pg_ed25519--0.2.sql

PG_CONFIG = pg_config
PGXS := $(shell $(PG_CONFIG) --pgxs)
include $(PGXS)
